#!/usr/bin/perl

# Copyright 2017 Aleisha Amohia <aleisha@catalyst.net.nz>
#
# This file is part of Koha
#
# Koha is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Koha is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Koha; if not, see <http://www.gnu.org/licenses>.

use Modern::Perl;

use Test::More tests => 11;

use Koha::Recalls;
use Koha::Recall;
use Koha::Database;
use Koha::DateUtils;
use C4::Circulation;
use t::lib::TestBuilder;

my $schema = Koha::Database->new->schema;
$schema->storage->txn_begin;

my $builder         = t::lib::TestBuilder->new;
my $library         = $builder->build( { source => 'Branch' } );
my $patron          = $builder->build( { source => 'Borrower', value => { branchcode => $library->{branchcode} } } );
my $biblio          = $builder->build( { source => 'Biblio' } );
my $itemtype        = $builder->build( { source => 'Itemtype' } );
my $item            = $builder->build( { source => 'Item', value => { biblionumber => $biblio->{biblionumber}, itype => $itemtype->{itemtype}, holdingbranch => $library->{branchcode} } } );
my $checkout        = $builder->build( { source => 'Issue', value => { borrowernumber => 5, itemnumber => $item->{itemnumber} } } );

# recall requested by second patron
my $recall = Koha::Recall->new({
    borrowernumber => $patron->{borrowernumber},
    recalldate => dt_from_string(),
    branchcode => $item->{holdingbranch},
    status => 'R',
    biblionumber => $biblio->{biblionumber},
    itemnumber => $item->{itemnumber},
    itemtype => $item->{itype},
})->store;

is( Koha::Recalls->search->count, 1, 'The one recall should be added' );
is( $recall->is_requested, 1, 'Recall status is set to R, requested' );

$recall->update({ status => 'O' });
is( $recall->is_overdue, 1, 'Recall is overdue to be returned' );

$recall->update({ status => 'W', waitingdate => dt_from_string() });
is( $recall->is_waiting, 1, 'Recall is waiting for pickup' );

$recall->update({ status => 'C', cancellationdate => dt_from_string(), old => 1 });
is( $recall->is_cancelled, 1, 'Recall has been cancelled' );

$recall->update({ status => 'E', expirationdate => dt_from_string(), old => 1 });
is( $recall->has_expired, 1, 'Recall has expired' );

$recall->update({ status => 'F', old => 1 });
is ($recall->is_finished, 1, 'Recall is closed');

is( $biblio->{biblionumber}, $recall->biblio->biblionumber, 'Can access biblio from recall' );

is( $patron->{borrowernumber}, $recall->patron->borrowernumber, 'Can access borrower from recall' );

is( $item->{itemnumber}, $recall->item->itemnumber, 'Can access item from recall' );

is( $library->{branchcode}, $recall->library->branchcode, 'Can access branch from recall' );

$schema->storage->txn_rollback;
