package Koha::Recall;

# Copyright 2017 Aleisha Amohia <aleisha@catalyst.net.nz>
#
# This file is part of Koha.
#
# Koha is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# Koha is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with Koha; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

use Modern::Perl;

use Koha::Database;

use base qw( Koha::Object );

=head1 NAME

Koha::Recall - Koha Recall Object class

=head1 API

=head2 Internal Methods

=cut

=head3 _type

=cut

sub _type {
    return 'Recall';
}

=head3 biblio

Returns the related Koha::Biblio object for this recall

=cut

sub biblio {
    my ($self) = @_;

    $self->{_biblio} ||= Koha::Biblios->find( $self->biblionumber() );

    return $self->{_biblio};
}

=head3 item

Returns the related Koha::Item object for this recall

=cut

sub item {

    my ($self) = @_;

    $self->{_item} ||= Koha::Items->find( $self->itemnumber() );

    return $self->{_item};
}


=head3 patron

Returns the related Koha::Patron object for this recall

=cut

sub patron {
    my ($self) = @_;
    $self->{_patron} ||= Koha::Patrons->find( $self->borrowernumber() );
    return $self->{_patron};
}

=head3 library

Returns the related Koha::Library object for this recall

=cut

sub library {
    my ($self) = @_;
    $self->{_library} ||= Koha::Libraries->find( $self->branchcode() );
    return $self->{_library};
}

=head3 checkout

Returns the related Koha::Checkout object for this recall

=cut

sub checkout {
    my ($self) = @_;
    $self->{_checkout} ||= Koha::Checkouts->find({ itemnumber => $self->itemnumber() });
    return $self->{_checkout};
}

=head3 is_waiting

Returns true if recall is awaiting pickup

=cut

sub is_waiting {
    my ($self) = @_;

    my $status = $self->status;
    return $status && $status eq 'W';
}

=head3 has_expired

Returns true if recall has expired

=cut

sub has_expired {
    my ($self) = @_;

    my $status = $self->status;
    return $status && $status eq 'E';
}

=head3 is_requested

Returns true if recall has been requested

=cut

sub is_requested {
    my ($self) = @_;

    my $status = $self->status;
    return $status && $status eq 'R';
}

=head3 is_overdue

Returns true if recall is overdue

=cut

sub is_overdue {
    my ($self) = @_;

    my $status = $self->status;
    return $status && $status eq 'O';
}

=head3 is_cancelled

Returns true if recall is cancelled

=cut

sub is_cancelled {
    my ($self) = @_;

    my $status = $self->status;
    return $status && $status eq 'C';
}

=head3 is_finished

Returns true if recall is closed

=cut

sub is_finished {
    my ($self) = @_;
    my $status = $self->status;
    return $status && $status eq 'F';
}

=head1 AUTHOR

Aleisha Amohia <aleisha@catalyst.net.nz>
Catalyst IT

=cut

1;
