#!/usr/bin/perl

# Copyright 2017 Aleisha Amohia <aleisha@catalyst.net.nz>
# This file is part of Koha.

# Koha is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
# Koha is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Koha; if not, see <http://www.gnu.org/licenses>.

use Modern::Perl;
use CGI qw ( -utf8 );
use C4::Auth;
use C4::Output;
use Koha::Recalls;
use Koha::Patrons;

my $input = CGI->new;
my ($template, $loggedinuser, $cookie)= get_template_and_user(
    {
       template_name => "members/recallshistory.tt",
       query => $input,
       type => "intranet",
       authnotrequired => 0,
       flagsrequired => {borrowers => 1},
       debug => 1,
    }
);

my $recalls = Koha::Recalls->search({ borrowernumber => $loggedinuser });
my $patron = Koha::Patrons->find($loggedinuser);
$template->param( picture => 1 ) if $patron->image;

$template->param(
        recalls         => $recalls,
        recallsview     => 1,
        borrowernumber  => $loggedinuser,
        title           => $patron->title,
        initials        => $patron->initials,
        surname         => $patron->surname,
        firstname       => $patron->firstname,
        cardnumber      => $patron->cardnumber,
        categorycode    => $patron->categorycode,
        category_type   => $patron->category->category_type,
        categoryname    => $patron->category->description,
        address         => $patron->address,
        streetnumber    => $patron->streetnumber,
        streettype      => $patron->streettype,
        address2        => $patron->address2,
        city            => $patron->city,
        zipcode         => $patron->zipcode,
        country         => $patron->country,
        phone           => $patron->phone,
        email           => $patron->email,
        branchcode      => $patron->branchcode
);

output_html_with_http_headers $input, $cookie, $template->output;
