#!/usr/bin/perl

# Copyright 2017 Aleisha Amohia <aleisha@catalyst.net.nz>
# This file is part of Koha.
# Koha is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.

# Koha is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Koha; if not, see <http://www.gnu.org/licenses>.

use Modern::Perl;
use CGI qw ( -utf8 );
use Encode qw( encode );
use C4::Auth;    # get_template_and_user
use C4::Output;
use Koha::Recalls;

my $input = new CGI;
my ( $template, $borrowernumber, $cookie ) = get_template_and_user(
   {
      template_name   => "opac-recalls.tt",
      query           => $input,
      type            => "opac",
      authnotrequired => 0,
   }
);

my $recalls = Koha::Recalls->search({ borrowernumber => $borrowernumber });
$template->param(
     RECALLS => $recalls,
     recallsview => 1
);

output_html_with_http_headers $input, $cookie, $template->output;
