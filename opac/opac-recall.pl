#!/usr/bin/perl

# This file is part of Koha.
#
# Copyright 2017 Aleisha Amohia <aleisha@catalyst.net.nz>
#
# Koha is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Koha is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Koha; if not, see <http://www.gnu.org/licenses>.

use Modern::Perl;
use CGI qw ( -utf8 );
use DateTime;
use C4::Auth;
use C4::Output;
use C4::Context;
use C4::Letters;
use Koha::Items;
use Koha::Recall;
use Koha::Recalls;
use Koha::DateUtils;
use Koha::IssuingRules;
use Koha::Patrons;

my $query = new CGI;
my ( $template, $borrowernumber, $cookie ) = get_template_and_user(
    {
        template_name   => "opac-recall.tt",
        query           => $query,
        type            => "opac",
        authnotrequired => 0,
    }
);

my $op = $query->param('op') || '';

my $itemnumber = $query->param('itemnumber');
my $item = Koha::Items->find($itemnumber);
my $biblio = Koha::Biblios->find($item->biblionumber);
my $patron = Koha::Patrons->find($borrowernumber);
my $recalled = Koha::Recalls->find({ itemnumber => $itemnumber });
my $error;

if ( defined $recalled && $recalled->borrowernumber == $borrowernumber && ($recalled->is_requested || $recalled->is_waiting || $recalled->is_overdue) ){
    # can't place a recall on an item that this borrower has already recalled
    # if recall is expired or cancelled, user may recall again
    $error = 'duplicate';
}

if ($op eq 'request'){
    if (!defined $borrowernumber){
    # can't place recall if not logged in
        $error = 'notloggedin';
        print $query->redirect('/cgi-bin/koha/opac-detail.pl?biblionumber='.$item->biblionumber);
    } elsif ( !defined $error ){
        # can recall
        my $borrower = Koha::Patrons->find($borrowernumber);
        my $issuing_rule = Koha::IssuingRules->get_effective_issuing_rule({ categorycode => $borrower->categorycode, itemtype => $item->itype, branchcode => $item->holdingbranch });
        my $recall_request = Koha::Recall->new({
            borrowernumber => $borrowernumber,
            recalldate => dt_from_string(),
            biblionumber => $biblio->biblionumber,
            branchcode => $item->holdingbranch,
            status => 'R',
            itemnumber => $itemnumber,
            itemtype => $item->itype,
        })->store;
        if (defined $recall_request->recall_id){ # successful recall
            my $recall = Koha::Recalls->find($recall_request->recall_id);
            # updating due date on checkout
            my $timestamp = dt_from_string($recall->timestamp);
            my $due_date;
            if ( $issuing_rule->recall_due_date_interval eq '' ) {
                $due_date = $timestamp->add( $issuing_rule->lengthunit => 0 );
            } else {
                $due_date = $timestamp->add( $issuing_rule->lengthunit => $issuing_rule->recall_due_date_interval );
            }
            my $checkout = Koha::Checkouts->find({ itemnumber => $itemnumber })->update({ date_due => $due_date });
            my $checkout_borrower = Koha::Patrons->find($checkout->borrowernumber->borrowernumber);

            # send notice to user with recalled item checked out
            my $letter = C4::Letters::GetPreparedLetter (
                module => 'circulation',
                letter_code => 'RETURN_RECALLED_ITEM',
                branchcode => $recall->branchcode,
                tables => {
                    'biblio', $biblio->biblionumber,
                    'borrowers', $checkout_borrower->borrowernumber,
                    'items', $itemnumber,
                    'issues', $itemnumber,
                },
            );
            C4::Message->enqueue($letter, $checkout_borrower->unblessed, 'email');

            $template->param(
                success => 1,
                due_interval => $issuing_rule->recall_due_date_interval,
                due_unit => $issuing_rule->lengthunit,
                due_date => $checkout->date_due
            );
        } else {
            $error = 'failed';
        }
    }
} elsif ($op eq 'cancel'){
    my $recall_id = $query->param('recall_id');
    Koha::Recalls->find($recall_id)->update({ cancellationdate => dt_from_string(), status => 'C', old => 1 });
    print $query->redirect('/cgi-bin/koha/opac-user.pl');
}

$template->param(
    biblio => $biblio,
    itemnumber => $itemnumber,
    error => $error
);

output_with_http_headers $query, $cookie, $template->output, 'html';
