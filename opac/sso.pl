#!/usr/bin/perl
#===============================================================================
#
#         FILE: sso.pl
#
#        USAGE: ./sso.pl
#
#  DESCRIPTION:
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Chris Cormack (rangi), chrisc@catalyst.net.nz
# ORGANIZATION: Koha Development Team
#      VERSION: 1.0
#      CREATED: 21/06/18 13:44:06
#     REVISION: ---
#===============================================================================

use strict;
use warnings;

use CGI;

my $input = new CGI;

my $url = $input->param('url');

if ( $url =~ /^https:\/\/toiohomai.mykoha.co.nz\// ) {
    print $input->redirect($url);
}
else {
    print $input->redirect(
        "https://toiohomai.mykoha.co.nz/cgi-bin/koha/opac-main.pl");
}
