$(document).ready(function() {

        $("#cancel_recall").click(function(e){
            if (confirmDelete(_("Are you sure you want to remove this recall?"))){
                var $self = $(this);
                var $recall_id = $(this).data('id');
                var ajaxData = {
                    'recall_id': $recall_id,
                };

                $.ajax({
                    url: '/cgi-bin/koha/svc/recall',
                    type: 'POST',
                    dataType: 'json',
                    data: ajaxData,
                })
                .done(function(data) {
                    var message = "";
                    if(data.success == 0) {
                        message = _("The recall may have already been cancelled. Please refresh the page.");
                    } else {
                        message = _("Cancelled");
                    }
                    $self.parent().html(message);
                });
            }
        });

        $("#recalls-table").dataTable($.extend(true, {}, dataTablesDefaults, {
            "aoColumnDefs": [
                { "aTargets": [ 'nosort' ], "bSortable": false, "bSearchable": false },
            ],
            "aaSorting": [[ 1, "asc" ]],
            "sPaginationType": "full_numbers"
        }));

        $("#cancel_selected").click(function(e){
            if ($("input[name='recall_ids']:checked").length > 0){
                return confirmDelete(_("Are you sure you want to remove the selected recall(s)?"));
            } else {
                alert(_("Please make a selection."));
            }
        });

        $("#select_all").click(function(){
            if ($("#select_all").prop("checked")){
                $("input[name='recall_ids']").prop("checked", true);
            } else {
                $("input[name='recall_ids']").prop("checked", false);
            }
        });

        $("#hide_old").click(function(){
            if ($("#hide_old").prop("checked")){
                $(".old").show();
            } else {
                $(".old").hide();
            }
        });
});
