<div id="recalls">
[% IF recalls.count %]
    <table id="recalls-table">
        <thead>
            <tr>
                <th class="nosort">&nbsp;</th>
                <th class="recall-borrower anti-the">Requested by</th>
                <th class="recall-title anti-the">Title</th>
                <th class="recall-barcode">Barcode</th>
                <th class="recall-date psort title-string">Placed on</th>
                <th class="recall-waiting psort title-string">Waiting since</th>
                <th class="recall-expiry title-string">Expires on</th>
                <th class="recall-branch">Pickup location</th>
                <th class="recall-status">Status</th>
                <th class="recall-due-date title-string">Due date</th>
                <th class="recall-cancel nosort">Cancel</th>
            </tr>
        </thead>

        <tbody>
            [% FOREACH recall IN recalls %]
                [% IF recall.is_cancelled || recall.has_expired || recall.is_finished %]
                    <tr class="old">
                [% ELSE %]
                    <tr>
                [% END %]
                    <td>
                        [% IF recall.is_requested || recall.is_waiting || recall.is_overdue %]
                            <input type="checkbox" value="[% recall.recall_id %]" name="recall_ids">
                        [% ELSE %]
                            &nbsp;
                        [% END %]
                    </td>
                    <td class="recall-borrower">
                        <a class="recall-borrower" href="/cgi-bin/koha/members/moremember.pl?borrowernumber=[% recall.borrowernumber %]">
                           [% recall.patron.firstname %] [% recall.patron.surname %] ([% recall.borrowernumber %])
                        </a>
                    </td>

                    <td class="recall-title">
                        <a class="recall-title" href="/cgi-bin/koha/catalogue/detail.pl?biblionumber=[% recall.biblionumber %]">
                            [% recall.biblio.title %]
                            [% FOREACH s IN recall.biblio.subtitles %]
                                [% s %]
                            [% END %]
                            [% recall.item.enumchron %]
                        </a>
                        [% recall.biblio.author %]
                    </td>

                    <td class="recall-barcode">
                        <a class="recall-title" href="/cgi-bin/koha/catalogue/detail.pl?biblionumber=[% recall.biblionumber %]">
                            [% recall.item.barcode %]
                        </a>
                    </td>

                    <td class="recall-date">
                        [% recall.recalldate | $KohaDates %]
                    </td>

                    <td class="recall-waiting">
                        [% IF recall.waitingdate %][% recall.waitingdate | $KohaDates %][% ELSE %]Not waiting[% END %]
                    </td>

                    <td class="recall-expiry">
                        [% IF ( recall.is_waiting ) %]
                            [% IF ( recall.expirationdate ) %]
                                [% recall.expirationdate | $KohaDates %]
                            [% ELSE %]
                                Never expires
                            [% END %]
                        [% ELSIF ( recall.has_expired && recall.expirationdate ) %]
                            [% recall.expirationdate | $KohaDates %]
                        [% ELSE %]
                            -
                        [% END %]
                    </td>

                    <td class="recall-branch">
                        [% recall.library.branchname %]
                    </td>

                    <td class="recall-status">
                        [% IF ( recall.is_requested ) %]
                            Requested
                        [% ELSIF ( recall.is_waiting ) %]
                            Ready for pickup
                        [% ELSIF ( recall.has_expired ) %]
                            Expired
                        [% ELSIF ( recall.is_cancelled ) %]
                            Cancelled
                        [% ELSIF ( recall.is_overdue ) %]
                            Overdue to be returned
                        [% ELSIF ( recall.is_finished ) %]
                            Closed
                        [% END %]
                    </td>

                    <td class="recall-due_date">
                        [% IF ( recall.is_requested ) %]
                            Due to be returned by [% recall.checkout.date_due | $KohaDates %]
                        [% ELSIF ( recall.is_waiting && recall.expirationdate ) %]
                            Pick up by [% recall.expirationdate | $KohaDates %]
                        [% ELSE %]
                            -
                        [% END %]
                    </td>

                    <td class="recall-cancel">
                        [% IF ( !recall.cancellationdate && ( recall.is_requested || recall.is_overdue ) ) %]
                            <a class="btn btn-xs btn-default" id="cancel_recall" data-id="[% recall.recall_id %]"><i class="fa fa-times"></i> Cancel</a>
                        [% END %]
                    </td>
                </tr>
            [% END %]
        </tbody>
    </table>
    [% ELSE %]
        No recalls have been made.
    [% END %]
</div>
