<div id="recalls">
[% IF recalls.count %]
    <table id="recalls-table">
        <thead>
            <tr>
                <th class="recall-title anti-the">Title</th>
                <th class="recall-date psort title-string">Placed on</th>
                <th class="recall-expiry title-string">Expires on</th>
                <th class="recall-branch">Pickup location</th>
                <th class="recall-status">Status</th>
                <th class="recall-cancel">Cancel</th>
            </tr>
        </thead>

        <tbody>
            [% FOREACH recall IN recalls %]
                <tr>
                    <td class="recall-title">
                        <a class="recall-title" href="/cgi-bin/koha/catalogue/detail.pl?biblionumber=[% recall.biblionumber %]">
                            [% recall.biblio.title %]
                            [% FOREACH s IN recall.biblio.subtitles %]
                                [% s %]
                            [% END %]
                            [% recall.item.enumchron %]
                        </a>
                        [% recall.biblio.author %]
                    </td>

                    <td class="recall-date">
                        [% recall.recalldate | $KohaDates %]
                    </td>

                    <td class="recall-expiry">
                        [% IF ( recall.is_waiting ) %]
                            [% IF ( recall.expirationdate ) %]
                                [% recall.expirationdate | $KohaDates %]
                            [% ELSE %]
                                Never expires
                            [% END %]
                        [% ELSIF ( recall.has_expired && recall.expirationdate ) %]
                            [% recall.expirationdate | $KohaDates %]
                        [% ELSE %]
                            -
                        [% END %]
                    </td>

                    <td class="recall-branch">
                        [% recall.library.branchname %]
                    </td>

                    <td class="recall-status">
                        [% IF ( recall.is_requested ) %]
                            Requested
                        [% ELSIF ( recall.is_waiting ) %]
                            Ready for pickup
                        [% ELSIF ( recall.has_expired ) %]
                            Expired
                        [% ELSIF ( recall.is_cancelled ) %]
                            Cancelled
                        [% ELSIF ( recall.is_overdue ) %]
                            Overdue to be returned
                        [% ELSIF ( recall.is_finished ) %]
                            Closed
                        [% END %]
                    </td>

                    <td class="recall-cancel">
                        [% IF ( !recall.cancellationdate && ( recall.is_requested || recall.is_overdue ) ) %]
                            <a class="btn btn-xs btn-default" id="cancel_recall" data-id="[% recall.recall_id %]"><i class="fa fa-times"></i> Cancel</a>
                        [% END %]
                    </td>

                </tr>
            [% END %]
        </tbody>
    </table>
    [% ELSE %]
        Patron has no current recalls.
    [% END %]
</div>
