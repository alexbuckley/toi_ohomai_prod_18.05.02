[% USE Koha %]
[% USE KohaDates %]
[% INCLUDE 'doc-head-open.inc' %]
<title>[% IF ( LibraryNameTitle ) %][% LibraryNameTitle %][% ELSE %]Koha online[% END %] catalog &rsaquo; Recall</title>
[% INCLUDE 'doc-head-close.inc' %]
[% BLOCK cssinclude %][% END %]
</head>
<body id="opac-recall" class="scrollto">
[% INCLUDE 'masthead.inc' %]

<div class="main">
    <ul class="breadcrumb">
        <li><a href="/cgi-bin/koha/opac-main.pl">Home</a> <span class="divider">&rsaquo;</span></li>
        <li><a href="/cgi-bin/koha/opac-detail.pl?biblionumber=[% biblio.biblionumber %]">Details for: [% biblio.title %]</a> <span class="divider">&rsaquo;</span></li>
        <li><a href="/cgi-bin/koha/opac-recall.pl?itemnumber=[% itemnumber %]">Recall item</a></li>
    </ul>

    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span2">
                <div id="navigation">
                    [% INCLUDE 'navigation.inc' IsPatronPage=1 %]
                </div>
            </div>
            <div class="span10">
                <div id="discharge" class="maincontainer">
                    <h1>Recall an item</h1>
                    [% IF Koha.Preference('UseRecalls') %]
                    [% IF error %]
                        <div class="dialog alert">
                            [% IF error == 'notloggedin' %]
                                Please log in to place a recall.
                            [% ELSIF error == 'duplicate' %]
                                You have already placed a recall on this item.
                            [% ELSE %]
                                An error has occurred while attempting to place a recall. Please contact your library.
                            [% END %]
                        </div>
                    [% END %]

                    [% IF success %]
                        <p>Your recall has been placed. The user the item is currently checked out to has been asked to return the item within [% due_interval %] [% due_unit %], on [% due_date | $KohaDates %].</p>
                        <p>You will be notified when your item is waiting to be picked up at the library.</p>
                    [% ELSIF not error %]
                        <p>All borrowable material is subject to recall if checked out and needed by someone else. We will ask the person who has checked out this item to return it so you may use it.</p>
<hr>
                        [% IF loggedinusername %]
                            <div class="dialog">
                            <p>Place a recall on [% biblio.title %] ([% biblio.author %])?</p> <a href="/cgi-bin/koha/opac-recall.pl?op=request&itemnumber=[% itemnumber %]" class="btn btn-default btn-sm">Confirm</a> &nbsp; <a href="/cgi-bin/koha/opac-detail.pl?biblionumber=[% biblio.biblionumber %]">Cancel</a>
                            </div>
                        [% ELSE %]
                            <div class="dialog alert">You must be logged in to place a recall.</div>
                        [% END %]

                    [% END %]
                    [% ELSE %]
                        Recalls have not been enabled. Please contact your library.
                    [% END %]
                </div> <!-- / #discharge -->
            </div> <!-- / .span10 -->
        </div> <!-- / .row-fluid -->
    </div>  <!-- / .container-fluid -->
</div> <!-- / .main -->

[% INCLUDE 'opac-bottom.inc' %]
[% BLOCK jsinclude %][% END %]
