#!/usr/bin/perl
#===============================================================================
#
#         FILE: preprocess_toiohomai_ozone.pl
#
#        USAGE: ./preprocess_toiohomai_ozone.pl
#
#  DESCRIPTION:
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Chris Cormack (rangi), chrisc@catalyst.net.nz
# ORGANIZATION: Koha Development Team
#      VERSION: 1.0
#      CREATED: 07/06/18 09:56:06
#     REVISION: ---
#===============================================================================

use strict;
use warnings;

use Text::CSV_XS;

my $csv = Text::CSV_XS->new(
    {
        binary             => 1,
        auto_diag          => 1,
        allow_whitespace   => 1,
        binary             => 1,
        allow_loose_quotes => 1,

    }
);
my $localtime = time();

open( my $processed, ">", "processed_$localtime" . ".csv" );

open( my $fh, "<-" );

my $headerline = $csv->getline($fh);

my $status = $csv->combine( @$headerline, 'branchcode' );
print $processed $csv->string . "\n";

while ( my $row = $csv->getline($fh) ) {
    my $attributes = $row->[25] || $row->[24];
    my $branchcode;
    if ( $attributes =~ /BRANCH:(\w*)/ ) {

        $branchcode = $1;
        if ($branchcode eq 'MOK'){
            $branchcode = 'MOKOIA';
        }
        if ($branchcode eq 'TAU'){
            $branchcode = 'DISTANCE';
        }
    }
    $status = $csv->combine( @$row, $branchcode );
    print $processed $csv->string . "\n";
}
