#!/bin/bash

ozone="Koha_CSV_Patron_Export.csv"
uow="Koha_CSV_Patron_no_userid_Export.csv"

export PERL5LIB=$PERL5LIB:/usr/share/koha/lib

cd /home/toftp/toiohomai_files
if [ -f "$ozone" ]
then
  echo "Preprocessing ozone"
  /usr/share/koha/bin/preprocess_toiohomai_ozone.pl < $ozone
  echo "Loading now"
  /usr/share/koha/bin/import_patrons.pl --file ./processed*  --matchpoint userid --default branchcode=BON --overwrite --verbose --verbose --confirm
  rm ./processed*
fi

if [ -f "$uow" ]
then
  echo "Preprocessing uow"
  /usr/share/koha/bin/preprocess_toiohomai_ozone.pl < $uow
  /usr/share/koha/bin/import_patrons.pl --file ./processed*  --matchpoint cardnumber --default branchcode=BON --overwrite --verbose --confirm
  rm ./processed*
fi
