#!/usr/bin/perl
#===============================================================================
#
#         FILE: toiohomai_photos.pl
#
#        USAGE: ./toiohomai_photos.pl
#
#  DESCRIPTION:
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Chris Cormack (rangi), chrisc@catalyst.net.nz
# ORGANIZATION: Koha Development Team
#      VERSION: 1.0
#      CREATED: 13/06/18 10:58:32
#     REVISION: ---
#===============================================================================

use strict;
use warnings;

use C4::Context;
use Archive::Extract;
use File::Copy;
use GD;
use Koha::Patrons;
use Koha::Patron::Images;

my $dbh = C4::Context->dbh();

my $sth = $dbh->prepare(
"SELECT cardnumber FROM borrowers,borrower_attributes WHERE borrowers.borrowernumber = borrower_attributes.borrowernumber AND code = 'IDENTITYID' AND attribute = ?"
);

my $archive = "Photos.zip";
my $dir     = "/home/toftp/toiohomai_files";

#deal with any zip files
if ( -e "$dir/$archive" ) {
    print "Unzipping now \n";
    my $ae = Archive::Extract->new( archive => "$dir/$archive" );

    my $ok = $ae->extract( to => "$dir/photos" ) or die $ae->error;

    # remove zip file
}

# deal with individual files, move them to photos/
opendir( my $dh, "$dir" ) or die "Can't opendir $dir: $!";
my @images = grep { /\.jpg$/ } readdir($dh);
closedir $dh;
print "Moving single files\n";

foreach my $image (@images) {
    move( "$dir/$image", "$dir/photos/$image" );
    print $image . "\n";
}

# now go through the photos dir, and create a file to match filenames to cardnumbers

opendir( $dh, "$dir/photos" ) or die "Can't opendir $dir/photos: $!";
@images = grep { /\.jpg$/ } readdir($dh);
closedir $dh;

my $debug;
foreach my $orig_image (@images) {
    print -M "$dir/photos/$orig_image";
    print "\n";
    if ( -M "$dir/photos/$orig_image" > 2 ) {
        next;
    }

    my $sort2 = $orig_image;
    $sort2 =~ s/\.jpg//;
    $sth->execute($sort2);
    my $row = $sth->fetchrow_hashref();
    if ($row) {
        my ( $srcimage, $image );
        if ( open( IMG, "$dir/photos/$orig_image" ) ) {
            $srcimage = GD::Image->new(*IMG);
            close(IMG);
            if ( defined $srcimage ) {
                my $imgfile;
                my $mimetype = 'image/png';
                my ( $width, $height ) = $srcimage->getBounds();
                if ( $width > 200 || $height > 300 ) {

                    # Percent we will reduce the image dimensions by...
                    my $percent_reduce;
                    if ( $width > 200 ) {

                     # If the width is oversize, scale based on width overage...
                        $percent_reduce = sprintf( "%.5f", ( 140 / $width ) );
                    }
                    else {
                        # otherwise scale based on height overage.
                        $percent_reduce = sprintf( "%.5f", ( 200 / $height ) );
                    }
                    my $width_reduce =
                      sprintf( "%.0f", ( $width * $percent_reduce ) );
                    my $height_reduce =
                      sprintf( "%.0f", ( $height * $percent_reduce ) );

                    #'1' creates true color image...
                    $image = GD::Image->new( $width_reduce, $height_reduce, 1 );
                    $image->copyResampled( $srcimage, 0, 0, 0, 0, $width_reduce,
                        $height_reduce, $width, $height );
                    $imgfile = $image->png();
                    undef $image;
                    undef $srcimage;    # This object can get big...
                }
                else {
                    $image   = $srcimage;
                    $imgfile = $image->png();
                    undef $image;
                    undef $srcimage;    # This object can get big...
                }
                $debug and warn "Image is of mimetype $mimetype";
                my $dberror;
                if ($mimetype) {
                    my $patron =
                      Koha::Patrons->find(
                        { cardnumber => $row->{cardnumber} } );
                    if ($patron) {
                        my $image = $patron->image;
                        $image ||= Koha::Patron::Image->new(
                            { borrowernumber => $patron->borrowernumber } );
                        $image->set(
                            {
                                mimetype  => $mimetype,
                                imagefile => $imgfile,
                            }
                        );
                        eval { $image->store };
                        if ($@) {

        # Errors from here on are fatal only to the import of a particular image
        #so don't bail, just note the error and keep going
                            warn "Database returned error: $@";
                        }
                        else {
                            # no error remove the file
                            print "Image added for "
                              . $row->{cardnumber} . "\n";

                  #                            unlink "$dir/photos/$orig_image";
                        }
                    }
                }
            }
        }
    }
    else {
        print "$orig_image not found\n";
    }
}
