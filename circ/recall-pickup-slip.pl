#!/usr/bin/perl


# Copyright 2018 Catalyst IT
#
# This file is part of Koha.
#
# Koha is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Koha is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Koha; if not, see <http://www.gnu.org/licenses>.

use Modern::Perl;
use C4::Context;
use C4::Output;
use CGI qw ( -utf8 );
use C4::Auth qw/:DEFAULT get_session/;
use Koha::Recall;
use Koha::Recalls;
use C4::Letters;
use C4::Message;
use Data::Dumper;
use Koha::Patrons;
use Koha::Items;
use Koha::Biblios;
use Koha::Libraries;
use vars qw($debug);

BEGIN {
    $debug = $ENV{DEBUG} || 0;
}

my $input = new CGI;
my $sessionID = $input->cookie("CGISESSID");
my $session = get_session($sessionID);

my $borrowernumber = $input->param('borrowernumber');
my $biblionumber = $input->param('biblionumber');
my $itemnumber = $input->param('itemnumber');
my $recallid = $input->param('recall_id');
my ( $template, $loggedinuser, $cookie ) = get_template_and_user(
    {
        template_name   => "circ/recall-pickup-slip.tt",
        query           => $input,
        type            => "intranet",
        authnotrequired => 0,
        flagsrequired   => { circulate => "circulate_remaining_permissions" },
        debug           => $debug,
    }
);

my $recall = Koha::Recalls->find($recallid);
my $patron = Koha::Patrons->find($borrowernumber);
my $item = Koha::Items->find($recall->itemnumber);
my $biblio = Koha::Biblios->find($item->biblionumber);
my $library = Koha::Libraries->find($recall->branchcode);

#Print slip to inform library staff of details of recall requester, so the item can be put aside for requester
my ($slip, $is_html);
my $letter = C4::Letters::GetPreparedLetter (
    module => 'circulation',
    letter_code => 'RECALL_REQUESTER_DET',
    tables => {
         'branches' => $library->branchcode,
         'borrowers' => $patron->borrowernumber,
         'biblio'   => $biblio->biblionumber,
         'items'   => $item->itemnumber,
         'recalls'  => $recall->recall_id
    }
);

if ($letter) {
    $slip = $letter->{content};
    $is_html = $letter->{is_html};
}

$template->param( slip => $slip ) if ($slip);
$template->param( plain => !$is_html );

output_html_with_http_headers $input, $cookie, $template->output;
