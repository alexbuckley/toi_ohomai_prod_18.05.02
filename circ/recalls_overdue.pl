#!/usr/bin/perl

# Copyright 2017 Aleisha Amohia <aleisha@catalyst.net.nz>

# This file is part of Koha.
#
# Koha is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.

# Koha is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Koha; if not, see <http://www.gnu.org/licenses>.

use Modern::Perl;
use CGI qw ( -utf8 );
use C4::Auth;
use C4::Output;
use Koha::Recalls;
use Koha::BiblioFrameworks;
use Koha::DateUtils;

my $query = new CGI;
my ( $template, $loggedinuser, $cookie, $flags ) = get_template_and_user(
    {
       template_name   => "circ/recalls_overdue.tt",
       query           => $query,
       type            => "intranet",
       authnotrequired => 0,
       flagsrequired   => { circulate => "circulate_remaining_permissions" },
       debug           => 1,
    }
);

my $op = $query->param('op') || 'list';
my @recall_ids = $query->multi_param('recall_ids');

if ($op eq 'cancel_multiple_recalls') {
    foreach my $id (@recall_ids) {
        Koha::Recalls->find($id)->update({ cancellationdate => dt_from_string(), status => 'C', old => 1 });
        $op = 'list'
    }
}

if ($op eq 'list') {
    my $recalls = Koha::Recalls->search({ status => 'O' });
    $template->param( recalls => $recalls );
}

# Checking if there is a Fast Cataloging Framework
$template->param( fast_cataloging => 1 ) if Koha::BiblioFrameworks->find( 'FA' );

# writing the template
output_html_with_http_headers $query, $cookie, $template->output;
